# DGTL-BRKPRG-2320
These files represent the Demo shared in DGTL-BRKPRG-2320 Cisco Live On-demand session
Chapter (3)

These files are for reference only. To actually rebuild this demo several tools and accounts are needed.

- Need a Linux (centos or ubuntu VM) to run containers. "DevBox"
- Python, Ansible, and pyATS/Genie from requirements.txt versions or later
- Gitlab container andubiel/gitlab
- NSO version 5.2 https://developer.cisco.com/site/nso/
    - See packages folder in repo for vxlan
- Action Orchestrator https://na.cloudcenter.cisco.com/
    - See eval account
    - refer to action_orchestrator/ for the demo workflow .json files 
- NGROK: This allows you to proxy ports from AO in the cloud to your CML2.0 lab
    - https://ngrok.com/
- Cisco Modeled Labs CML 2.0 
    -  Need image ISO that includes sdwan 
    -  See cml2_virl_toplogy for Demo topology
    - Details on CMLv2 here: https://developer.cisco.com/modeling-labs/



